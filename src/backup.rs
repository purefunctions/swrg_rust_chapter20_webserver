use std::{net::TcpListener, thread::sleep, time::Duration};
use std::{net::TcpListener, thread::sleep, time::Duration};
usue std::{net::TcpListener, thread::sleep, time::Duration};
use std::net::TcpStream;
use std::io::Read;
use std::io::Write;
use std::io;
use std::fs;

fn main2() -> io::Result<()> {
    let listener = TcpListener::bind("127.0.0.1:8888")?;
    println!("Socket bound");

    for stream in listener.incoming() {
        let stream = stream?;
        handle_connection(stream)?;
    }
    
    Ok(())
}
fn main1() {
    let listener;
    match TcpListener::bind("127.0.0.1:8888") {
        Ok(val) => listener = val,
        Err(err) => panic!(format!("{:?}",err.kind()))
    }
    println!("Socket bound");

    for stream in listener.incoming() {
        let stream = stream.unwrap();
        handle_connection(stream).unwrap();
    }
}

fn handle_connection(mut stream: TcpStream) -> io::Result<()> {
    println!("Inbound connection established");
    let mut input_buffer = [0; 2048];
    let num_bytes_read = stream.read(&mut input_buffer)?;
    println!("{} bytes read", num_bytes_read);
    let output_buffer = prepare_response(&input_buffer);
    let num_bytes_written = stream.write(&output_buffer)?;
    println!("{} bytes written", num_bytes_written);
    Ok(())
}

fn prepare_response(input: &[u8]) -> Vec<u8> {
    let utf8_input = String::from_utf8_lossy(&input);
    println!("{}", utf8_input);
    if let Some((method, url)) = extract_method_and_url(&utf8_input) {
        match (method.as_str(), url.as_str()) {
            ("GET", "/") => {
                let contents = fs::read("src/hello.html").unwrap();
                ok_response(&contents)
            },
            ("GET", "/sleep") => {
                sleep(Duration::from_secs(10));
                ok_response("Sleeping over".as_bytes())
            }
            _ => not_found_response("".as_bytes())
        }
    } else {
        bad_request_response("".as_bytes())
    }
}


type Method = String;
type URL = String;

fn extract_method_and_url(input: &str) -> Option<(Method, URL)> {
    parse_request_start_line(input.split("\r\n").next()?)
}

fn parse_request_start_line(line: &str) -> Option<(Method, String)> {
    let mut parts = line.split_whitespace();
    let method = parts.next()?;
    let url = parts.next()?;
    Option::Some((String::from(method).trim().to_uppercase(), String::from(url)))
}

fn ok_response(body: &[u8]) -> Vec<u8> {
    let mut response = String::from("HTTP/1.1 200 OK\r\n").as_bytes().to_vec();
    add_body(&mut response, body);
    response
}

fn not_found_response(body: &[u8]) -> Vec<u8> {
    let mut response = String::from("HTTP/1.1 404 NOT FOUND\r\n").as_bytes().to_vec();
    add_body(&mut response, body);
    response
}

fn bad_request_response(body: &[u8]) -> Vec<u8> {
    let mut response = String::from("HTTP/1.1 400 BAD REQUEST\r\n").as_bytes().to_vec();
    add_body(&mut response, body);
    response
}

fn add_body(response: &mut Vec<u8>, body: &[u8]) {
    if body.len() > 0 {
        response.extend(format!("Content-Length: {}\r\n\r\n", body.len()).as_bytes());
        response.extend(body);
    }
}
use std::net::TcpStream;
use std::io::Read;
use std::io::Write;
use std::io;
use std::fs;

fn main2() -> io::Result<()> {
    let listener = TcpListener::bind("127.0.0.1:8888")?;
    println!("Socket bound");

    for stream in listener.incoming() {
        let stream = stream?;
        handle_connection(stream)?;
    }
    
    Ok(())
}
fn main1() {
    let listener;
    match TcpListener::bind("127.0.0.1:8888") {
        Ok(val) => listener = val,
        Err(err) => panic!(format!("{:?}",err.kind()))
    }
    println!("Socket bound");

    for stream in listener.incoming() {
        let stream = stream.unwrap();
        handle_connection(stream).unwrap();
    }
}

fn handle_connection(mut stream: TcpStream) -> io::Result<()> {
    println!("Inbound connection established");
    let mut input_buffer = [0; 2048];
    let num_bytes_read = stream.read(&mut input_buffer)?;
    println!("{} bytes read", num_bytes_read);
    let output_buffer = prepare_response(&input_buffer);
    let num_bytes_written = stream.write(&output_buffer)?;
    println!("{} bytes written", num_bytes_written);
    Ok(())
}

fn prepare_response(input: &[u8]) -> Vec<u8> {
    let utf8_input = String::from_utf8_lossy(&input);
    println!("{}", utf8_input);
    if let Some((method, url)) = extract_method_and_url(&utf8_input) {
        match (method.as_str(), url.as_str()) {
            ("GET", "/") => {
                let contents = fs::read("src/hello.html").unwrap();
                ok_response(&contents)
            },
            ("GET", "/sleep") => {
                sleep(Duration::from_secs(10));
                ok_response("Sleeping over".as_bytes())
            }
            _ => not_found_response("".as_bytes())
        }
    } else {
        bad_request_response("".as_bytes())
    }
}


type Method = String;
type URL = String;

fn extract_method_and_url(input: &str) -> Option<(Method, URL)> {
    parse_request_start_line(input.split("\r\n").next()?)
}

fn parse_request_start_line(line: &str) -> Option<(Method, String)> {
    let mut parts = line.split_whitespace();
    let method = parts.next()?;
    let url = parts.next()?;
    Option::Some((String::from(method).trim().to_uppercase(), String::from(url)))
}

fn ok_response(body: &[u8]) -> Vec<u8> {
    let mut response = String::from("HTTP/1.1 200 OK\r\n").as_bytes().to_vec();
    add_body(&mut response, body);
    response
}

fn not_found_response(body: &[u8]) -> Vec<u8> {
    let mut response = String::from("HTTP/1.1 404 NOT FOUND\r\n").as_bytes().to_vec();
    add_body(&mut response, body);
    response
}

fn bad_request_response(body: &[u8]) -> Vec<u8> {
    let mut response = String::from("HTTP/1.1 400 BAD REQUEST\r\n").as_bytes().to_vec();
    add_body(&mut response, body);
    response
}

fn add_body(response: &mut Vec<u8>, body: &[u8]) {
    if body.len() > 0 {
        response.extend(format!("Content-Length: {}\r\n\r\n", body.len()).as_bytes());
        response.extend(body);
    }
}
use std::net::TcpStream;
use std::io::Read;
use std::io::Write;
use std::io;
use std::fs;

fn main2() -> io::Result<()> {
    let listener = TcpListener::bind("127.0.0.1:8888")?;
    println!("Socket bound");

    for stream in listener.incoming() {
        let stream = stream?;
        handle_connection(stream)?;
    }
    
    Ok(())
}
fn main1() {
    let listener;
    match TcpListener::bind("127.0.0.1:8888") {
        Ok(val) => listener = val,
        Err(err) => panic!(format!("{:?}",err.kind()))
    }
    println!("Socket bound");

    for stream in listener.incoming() {
        let stream = stream.unwrap();
        handle_connection(stream).unwrap();
    }
}

fn handle_connection(mut stream: TcpStream) -> io::Result<()> {
    println!("Inbound connection established");
    let mut input_buffer = [0; 2048];
    let num_bytes_read = stream.read(&mut input_buffer)?;
    println!("{} bytes read", num_bytes_read);
    let output_buffer = prepare_response(&input_buffer);
    let num_bytes_written = stream.write(&output_buffer)?;
    println!("{} bytes written", num_bytes_written);
    Ok(())
}

fn prepare_response(input: &[u8]) -> Vec<u8> {
    let utf8_input = String::from_utf8_lossy(&input);
    println!("{}", utf8_input);
    if let Some((method, url)) = extract_method_and_url(&utf8_input) {
        match (method.as_str(), url.as_str()) {
            ("GET", "/") => {
                let contents = fs::read("src/hello.html").unwrap();
                ok_response(&contents)
            },
            ("GET", "/sleep") => {
                sleep(Duration::from_secs(10));
                ok_response("Sleeping over".as_bytes())
            }
            _ => not_found_response("".as_bytes())
        }
    } else {
        bad_request_response("".as_bytes())
    }
}


type Method = String;
type URL = String;

fn extract_method_and_url(input: &str) -> Option<(Method, URL)> {
    parse_request_start_line(input.split("\r\n").next()?)
}

fn parse_request_start_line(line: &str) -> Option<(Method, String)> {
    let mut parts = line.split_whitespace();
    let method = parts.next()?;
    let url = parts.next()?;
    Option::Some((String::from(method).trim().to_uppercase(), String::from(url)))
}

fn ok_response(body: &[u8]) -> Vec<u8> {
    let mut response = String::from("HTTP/1.1 200 OK\r\n").as_bytes().to_vec();
    add_body(&mut response, body);
    response
}

fn not_found_response(body: &[u8]) -> Vec<u8> {
    let mut response = String::from("HTTP/1.1 404 NOT FOUND\r\n").as_bytes().to_vec();
    add_body(&mut response, body);
    response
}

fn bad_request_response(body: &[u8]) -> Vec<u8> {
    let mut response = String::from("HTTP/1.1 400 BAD REQUEST\r\n").as_bytes().to_vec();
    add_body(&mut response, body);
    response
}

fn add_body(response: &mut Vec<u8>, body: &[u8]) {
    if body.len() > 0 {
        response.extend(format!("Content-Length: {}\r\n\r\n", body.len()).as_bytes());
        response.extend(body);
    }
}