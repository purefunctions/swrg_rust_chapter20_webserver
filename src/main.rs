use std::net::TcpListener;
use std::net::TcpStream;
use std::io::Read;
use std::io::Write;
use std::fs;


fn main() {
    let listener = match TcpListener::bind("127.0.0.1:8888") {
        Ok(val) => val,
        Err(err) => panic!(format!("Bind error: {:?}", err.kind()))
    };

    println!("Socket bound");

    for stream in listener.incoming() {
        let stream = stream.unwrap();
        println!("Incoming connection success");
        handle_connection(stream);
    }
}

fn handle_connection(mut stream: TcpStream) {
    let mut input = [0; 2048];
    stream.read(&mut input[..]).unwrap();
    // HTTP/1.1 200 OK
    // Content-Length: <num>
    //
    // body
    let request = String::from_utf8_lossy(&input[..]);
    let response;
    if request.starts_with("GET / HTTP/1.1") {
        let contents = fs::read_to_string("src/hello.html").unwrap();
        response = format!("HTTP/1.1 200 OK\r\nContent-Length: {}\r\n\r\n{}", contents.len(), contents);
    } else {
        response = String::from("HTTP/1.1 404 NOT FOUND\r\n\r\n");
    }

    stream.write(response.as_bytes()).unwrap();
    stream.flush().unwrap();
}